import requests
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive.number import Float
from spyne.model.primitive.string import String
from spyne.service import ServiceBase
from spyne.server.wsgi import WsgiApplication
from spyne.protocol.soap import Soap11
from spyne.model.complex import Iterable
from spyne import Unicode, Integer, AnyDict
from wsgiref.simple_server import make_server
from geopy.geocoders import Nominatim
from geopy import location, distance, geocoders
from geopy.extra.rate_limiter import RateLimiter
from geographiclib.geodesic import Geodesic
import geopy

API_URL = "https://opendata.reseaux-energies.fr/api/records/1.0/search/"


class SoapService(ServiceBase):
    @rpc(Float, Float, Float, Float, _returns=Float)
    def calculDistance(ctx, lat1, lon1, lat2, lon2):
        print(f'SOAP : {lat1} | {lon1} | {lat2} | {lon2}')
        address1 = (lat1, lon1)
        address2 = (lat2, lon2)
        result = round((geopy.distance.distance(address1, address2).km), 2)
        return result


    @rpc(Float, Float, Float, Float, Integer, Integer, _returns=Iterable(String))
    def get_all_points(ctx, lat1, lon1, lat2, lon2, autonomie, nb_recharge):
        
        ### trouver points de recherche bornes
        geod = Geodesic.WGS84
        inv = geod.Inverse(float(lat1), float(lon1), float(lat2), float(lon2))
        azi1 = inv['azi1']

        all_research_points = []
        bornes = []
    

        if nb_recharge == 0:
            bornes.append('Pas de recharge nécessaire')
        else:
            for i in range (1, nb_recharge+1):
                s = int(autonomie)*800

                ### calcul coord voiture au bout de s km de trajet
                research_point = geod.Direct(float(lat1), float(lon1), azi1, s*i)
                all_research_points.append(f"'lat': {research_point['lat2']}, 'lon': {research_point['lon2']}")

                ### appel api pour borne la plus proche
                api_point_lat = research_point['lat2']
                api_point_lon = research_point['lon2']
                api_point_radius = int(autonomie)*200

                api_parameters = {
                    "dataset": "bornes-irve",
                    "timezone": "UTC",
                    "rows": 3,
                    "start": 0,
                    "q": "",
                    "format": "json",
                    "geofilter.distance": [
                        ', '.join(list(map(str, [api_point_lat, api_point_lon, api_point_radius])))
                    ]
                }

                api_response = requests.get(API_URL, params=api_parameters)
                #print(api_response.text)

                dic = api_response.json()
                if int(dic['nhits']) == 0:
                    bornes.append('PAS DE BORNE À PROXIMITÉ') #return # Pas de borne
                else:
                    bornes.append(dic['records'][0]['fields']['ad_station'])
                #print(bornes)

        return bornes



### instanciation d'une appli
application = Application([SoapService], 'spyne.examples.hello.soap',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11())
wsgi_application = WsgiApplication(application)

### adresse http://127.0.0.1:8000/?wsdl
server = make_server('127.0.0.1', 8000, wsgi_application)
server.serve_forever()