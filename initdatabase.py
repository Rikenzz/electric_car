import sqlite3
from pathlib import Path

DATABASE = './database/voitures.db'
        
def init():
    conn = sqlite3.connect(DATABASE)
    return conn

def returnPath(DATABASE):
    return DATABASE
    
def end(conn):
    conn.close()

def dbCreate():
    conn = init()
    initVoiture(conn)
    return conn


### TABLES ###

def initVoiture(conn):
    cursor = conn.cursor()
    cursor.execute("""
                   CREATE TABLE IF NOT EXISTS voitures(
                           id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                           model TEXT,
                           autonomie TEXT,
                           recharge TEXT
                           )
                   """)
    conn.commit()


# dbCreate()