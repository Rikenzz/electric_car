from flask import Flask, jsonify, request, render_template
from geopy import location, distance, geocoders
from geopy.geocoders import Nominatim
from geographiclib.geodesic import Geodesic
from geopy.extra.rate_limiter import RateLimiter
import json, requests, sqlite3, geopy
from zeep import Client
from flask_googlemaps import GoogleMaps, Map
from urllib.parse import unquote

import dbmanage


API_URL = "https://opendata.reseaux-energies.fr/api/records/1.0/search/"


### FUNCTIONS ###

client = Client('http://127.0.0.1:8000/?wsdl')

### ROUTES ###

app = Flask(__name__)

app.config['GOOGLEMAPS_KEY'] = "AIzaSyBcc80Jeyig5Kbd0lXzV4-6e5xWRgsc0kc"
GoogleMaps(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    con = dbmanage.connect()
    con.row_factory = sqlite3.Row
    voitures = dbmanage.selectAll(con)

    return render_template('index.html', voitures=voitures)


@app.route('/travel_distance', methods=['GET', 'POST'])
def travel_distance():
    ### recuperation data formulaire
    address_depart = request.form['form_depart']
    address_arrivee = request.form['form_arrivee']
    voitures_model = request.form['voitures']

    ### recuperation coordonnees depart et arrivee
    geolocator =  Nominatim(user_agent="voyage_electrique")
    geocode = RateLimiter(geolocator.geocode, min_delay_seconds=1, max_retries=0)

    location_depart = geocode(address_depart)
    render_depart = {
        "render_address": address_depart,
        "render_latitude": location_depart.latitude,
        "render_longitude": location_depart.longitude
    }

    location_arrivee = geocode(address_arrivee)
    render_arrivee = {
        "render_address": address_arrivee,
        "render_latitude": location_arrivee.latitude,
        "render_longitude": location_arrivee.longitude
    }


    ### recuperation voiture dans db
    conn = dbmanage.connect()
    conn.row_factory = sqlite3.Row
    voiture = dbmanage.selectCar(conn,voitures_model)
        # id = voiture[0][0]
        # model = voiture[0][1]
    autonomie = int(voiture[0][2])
    recharge = int(voiture[0][3])
    conn.close()


    ### appel soap calcul distance depart-arrivee a vol d'oiseau
    distance = client.service.calculDistance(location_depart.latitude, location_depart.longitude, location_arrivee.latitude, location_arrivee.longitude)

    ### calcul du temps de trajet et rechargement
    nb_recharge = int(int(distance) / (autonomie*0.8))
    tps_recharge_mn = recharge*nb_recharge
    tps_trajet_mn = round((distance/90)*60) + tps_recharge_mn

    tps_recharge_h = 0
    tps_trajet_h = 0

    ### processing time: recharge
    while tps_recharge_mn >= 60:
        tps_recharge_mn = tps_recharge_mn - 60
        tps_recharge_h = tps_recharge_h + 1
        # print(f'Recharge: {tps_recharge_h}h {tps_recharge_mn}mn')
    if tps_recharge_h == 0:
        tps_recharge = (f"{tps_recharge_mn}mn")
    else:
        tps_recharge = (f"{tps_recharge_h}h {tps_recharge_mn}mn")

    ### processing time: trajet
    while tps_trajet_mn >= 60:
        tps_trajet_mn = tps_trajet_mn - 60
        tps_trajet_h = tps_trajet_h + 1
    if tps_trajet_h == 0:
        tps_trajet = (f"{tps_trajet_mn}mn")
    else:
        tps_trajet = (f"{tps_trajet_h}h {tps_trajet_mn}mn")

    ### appel soap recuperation coordonnees bornes
    bornes = client.service.get_all_points(location_depart.latitude, location_depart.longitude, location_arrivee.latitude, location_arrivee.longitude, autonomie, nb_recharge)

    ### creation map
    # waypoints = [{location:{lat:46.416748, lng:2.379690}}];
    waypoints = []
    # for borne in bornes:
    #     waypoints.append()

    return render_template('travel_distance.html', depart=render_depart, arrivee=render_arrivee, voitures=voiture, distance=distance, bornes=bornes, tps_trajet=tps_trajet, tps_recharge=tps_recharge, waypoints=waypoints)


@app.route('/api', methods=['GET'])
def api():
	response = requests.get(API_URL)
	content = json.loads(response.content.decode("utf-8"))
	return content



if __name__ == '__main__':
    app.run(debug=True)