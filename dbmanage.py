import sqlite3
import initdatabase as initdB
from pathlib import Path


def connect():
    my_file = Path(initdB.DATABASE)

    if my_file.is_file():
        conn = initdB.init()
    else:
        conn = initdB.dbCreate()
    return conn


def addDevice(model, autonomie, recharge, conn):
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO voitures(model, autonomie, recharge) VALUES(?,?,?)""", (model, autonomie, recharge))
    conn.commit()
    

def selectAll(conn):
    cursor = conn.cursor()
    cursor.execute("""SELECT * FROM voitures""")
    allDevice = cursor.fetchall()
    return allDevice


def selectCar(conn, model):
    cursor = conn.cursor()
    cursor.execute("""SELECT * FROM voitures WHERE model = '"""+model+"""'""")
    voiture = cursor.fetchall()
    return voiture


conn = connect()
# addDevice("Tesla Model S", 350, 60, conn)
# addDevice("Renault Zoe", 200, 40, conn)
# addDevice("Peugeot e-2008", 300, 70, conn)
initdB.end(conn)